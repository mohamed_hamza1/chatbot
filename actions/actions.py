from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, FollowupAction,Restarted
from .APIs import APIService
import os
from dotenv import load_dotenv
import re
import arabic_reshaper
from bidi.algorithm import get_display

load_dotenv()

header_request_json = {}
is_Arabic = False
class Validation:
    def __init__(self):
        # You can add initialization code here if needed
        pass
    def is_arabic(self, text):
        # Reshape the Arabic text
        reshaped_text = arabic_reshaper.reshape(text)
        # Check if the reshaped text contains Arabic characters
        return any('\u0600' <= char <= '\u06FF' for char in reshaped_text)

    def is_saudi_phone_number(self, phone_number):
        # Regular expression for Saudi phone numbers
        pattern = re.compile(r'(\+9665\d{8}|009665\d{8}|05\d{8})$')

        # Remove spaces, dashes, and parentheses which are common in formatting
        phone_number = phone_number.replace(" ", "").replace("-", "").replace("(", "").replace(")", "")

        # Check if the phone number matches the pattern
        return bool(pattern.match(phone_number))
class ActionAskPhoneNumber(Action):
    def name(self) -> Text:
        return "action_ask_phone_number"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        print("Hello ",tracker.sender_id)
        global header_request_json
        dispatcher.utter_message(text="Hello and welcome to Extra's service assistant! Please enter your Mobile Phone to get started.")
        header_request_json = {}
        return []
    

class ActionAskCity(Action):
    def name(self) -> Text:
        return "action_ask_city"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        
        global header_request_json
        first_intent = tracker.latest_message.get('text')
        
        latest_message = "00966533168706"
        validator = Validation()
        if not validator.is_saudi_phone_number(latest_message):
            dispatcher.utter_message(text="Kindly enter a valid phone number from Saudi Arabia")
            FollowupAction("utter_greet")
            return[]
        
        service_request_service = APIService(os.getenv("base_url"))
        print(latest_message,os.getenv("base_url") )
        user_requests_response = service_request_service.verify_code(latest_message,1,"",123456)
        if user_requests_response["ErrorMessage"] is None:
            first_name = user_requests_response["Data"]["first_name"]
            customer_id = user_requests_response["Data"]["Id"]
            print(customer_id)
           
            user_Adress_response = service_request_service.get_customer_addresses(user_requests_response["Data"]["Id"],user_requests_response["Data"]["Token"])
            adress_selected = user_Adress_response["Data"][0]["AddressLine_1"]
            # dispatcher.utter_message(text=f"Hi {first_name},\nPlease select your city: \n\n\u20221- Riyadh\n\n\u20222- Abha\n\n\u20223- Jeddah\n\n\u20224-etc.")
            print(first_intent)
            global is_Arabic
            is_Arabic = True
            if is_Arabic:
                print("arabic")
                dispatcher.utter_message(text=f"مرحباً باسم")
                dispatcher.utter_message(text=f"هل يمكنك تأكيد ما إذا كان العنوان التالي هو العنوان الصحيح للخدمات التي تحتاجها؟")
                dispatcher.utter_message(text=f"\n\n\u2025{adress_selected}")
            else:
                print("not arabic")
                dispatcher.utter_message(text=f"Hi *{first_name}*, Could you please confirm if the following is the correct address for the services you require?\n\n\u2025{adress_selected}")
            header_request_json["user"] = {
                "Token": user_requests_response["Data"]["Token"],
                "MobileNumber": user_requests_response["Data"]["Id"],
                "Address": user_Adress_response["Data"][0]
            } 
        else:
            dispatcher.utter_message(text=f"I regret to inform you that the provided number is not currently registered with our services. As a result, this chat session will be restarted.\nThank you for your understanding and kind regards,\nExtra Customer Services")
            return[Restarted()]
        return []

class ActionAskArea(Action):
    def name(self) -> Text:
        return "action_ask_area"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        
        latest_message = tracker.latest_message.get('text')
        service_request_service = APIService(os.getenv("base_url"))
        user_requests_response = service_request_service.get_mappingDetails_City(latest_message)

        if user_requests_response["Data"]["Areas"] is None:
            dispatcher.utter_message(text=f"Our services are exclusively available in Saudi Arabia.")
        else:
            if user_requests_response["Data"]["City"]["Covered"] is False:
                dispatcher.utter_message(text=f"Sorry, our services aren't covering {latest_message} rightnow.")
                return[Restarted()]
            else:
                result_areas=""
                
                areas = user_requests_response["Data"]["Areas"]
                for i, area in enumerate(areas):
                    if i < 5:  # Loop only for the first five elements
                        result_areas +="\u2022" +str(area["DetailName"]) + "\n\n" 
                dispatcher.utter_message(text=f"Please Choose the Area")#of {latest_message},\n\n{result_areas}etc..
        return []

class ActionAskService(Action):
    def name(self) -> Text:
        return "action_ask_service"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text="من فضلك حدد الخدمة التي تريدها")
        else:   
            dispatcher.utter_message(text="Please select service type")
        return []

class ActionAskAcService(Action):
    def name(self) -> Text:
        return "action_ask_ac_service"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        global is_Arabic
        
        if is_Arabic:
            dispatcher.utter_message(text="ما نوع خدمة المكيفات المطلوبة؟")
        else:
            dispatcher.utter_message(text="What Kind of A/C Services do you want?")
       
        return []

class ActionAskElectronicsService(Action):
    def name(self) -> Text:
        return "action_ask_Electronics_service"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text="بالنسبة للخدمات الإلكترونية، أي نوع من الخدمات تحتاج إليه؟")
        else:
            dispatcher.utter_message(text="For electronic services, what type of services are you in need of?")
       
        return []

class ActionShowPackages(Action):
    def name(self) -> Text:
        return "action_show_packages"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # Here you would normally include logic to display an image of the packages.
        # For simplicity, we will just send a text message.
        latest_message = tracker.latest_message.get('text')
        print(latest_message)
        dispatcher.utter_message(text="Here are our Pro Clean Service packages. Would you like to continue?")
        return []

class ActionAskDevice(Action):
    def name(self) -> Text:
        return "action_ask_device"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text="هل تفضل اختيار جهاز من الأجهزة المسجلة لديك، أم تفضل إضافة جهاز جديد؟")
        else:
            dispatcher.utter_message(text="Would you like to choose from your saved devices, or would you prefer to add a new one?")
        
        return []

class ActionDeviceDetails(Action):
    def name(self) -> Text:
        return "action_device_details"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # Here, you would pull device details from a database or similar.
        # We will use a placeholder response.
        global header_request_json
        service_request_service = APIService(os.getenv("base_url"))
        user_requests_response = service_request_service.get_user_devices(header_request_json["user"]["MobileNumber"],"AIR CONDITIONER-402",header_request_json["user"]["Token"])
        result_devices=""
        
        devices = user_requests_response["Data"]
        
        for device in devices:
            print("Brand - "+device["x_brand"]+", Model"+device["model_cd"])
            result_devices +="\u2022" +str("Brand - "+device["x_brand"]+", Model - "+device["model_cd"]) + "\n\n" 
            
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text=f"تفاصيل الاجهزة المسجلة لديك\n\n{result_devices}")
        else:
            dispatcher.utter_message(text=f"Your availble devices details:\n\n{result_devices}")
            

        return []

class ActionAskForDescription(Action):
    def name(self) -> Text:
        return "action_ask_for_description"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text="يرجى تقديم وصف للخدمة التي تحتاجها")
        else:
            dispatcher.utter_message(text="Please provide a description of the service you need.")
        return []

class ActionAskToAttachImage(Action):
    def name(self) -> Text:
        return "action_ask_to_attach_image"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Could you attach an image related to the service?")
        return []

class ActionAskForAnotherDevice(Action):
    def name(self) -> Text:
        return "action_ask_for_another_device"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        latest_message = tracker.latest_message.get('text')
        global header_request_json
        header_request_json["user"]["Service_Description"] = latest_message
        global is_Arabic
        print("Another Device")
        if is_Arabic:
            dispatcher.utter_message(text="هل ترغب في اختيار جهاز جديد او الاستمرار في عملية الدفع؟")
        else:
            dispatcher.utter_message(text="Would you like to add another device or continue to payment?")
            
        return []

class ActionAskPaymentMethod(Action):
    def name(self) -> Text:
        return "action_ask_payment_method"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text="هل ترغب في الدفع عبر الإنترنت أم نقدًا؟")
        else:    
            dispatcher.utter_message(text="Do you want to pay online or by cash?")
        
        return []

class ActionSendInvoice(Action):
    def name(self) -> Text:
        return "action_send_invoice"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # Here, you would normally generate and send an invoice.
        # For simplicity, we will just send a text message.
        global header_request_json
        token = header_request_json["user"]["Token"]
        description = header_request_json["user"]["Service_Description"]
        adress = header_request_json["user"]["Address"]
        service_request_service = APIService(os.getenv("base_url"))
        request = {"Model": "CLP18E6XL", "ServiceTypeName": "AC", "ServiceTypeId":"110a4f2d-ae59-4ac6-b820-88011834819a", "ItemTypeName": "Pro Clean Services", "ItemTypeCode": "A-PRO_C", "ItemTypeId": "eff607b3-74f1-4a55-84fd-b6deb00697c1", "MobileNumber": "00966533168706", "ContractNumber": "", "InvoiveNumber": "20181229110081026037", "Brand": "CLASSP", "BrandId": "556e1177-5622-4d22-80e3-cf340ec030a1", "AddressLine1": "00966533168706 - Saudi Arabia - Jeddah - Abhur ash shamaliyah", "ItemDefectType": "Hardware", "ItemDefectTypeId": "", "Symptoms": "", "SymptomId": "", "SymptomDescription": description, "CustomerRequest": "", "ProductName":"خدمة تنظيف المكيفات", "ProductAssetNumber": "20181229110081026037-0", "ImagesPathes": [], "PackageId": "الأساسية (159 ريال)", "RequestKey": "62618fc0-a26d-11ee-bb83-93bf49d31aa9"}
        service_request_data = {
            "Requests":[
                request
            ],
            "CustomerAddress": adress
        } 
        print(service_request_data)
        user_requests_response = service_request_service.create_service_request(service_request_data,token)
        print(user_requests_response)
        SR_Number = user_requests_response["Data"][0]["P_SR_NO"]
        global is_Arabic
        if is_Arabic:
            dispatcher.utter_message(text=f"تم إنشاء طلب الخدمة الخاص بك. يرجى حفظ رقم طلب الخدمة التالي\n\u2022{SR_Number}")
        else:    
            dispatcher.utter_message(text=f"The Request for your service has been created, save the following service request number\n\u2022{SR_Number}")
        return []

class ActionResetConversation(Action):
    def name(self) -> str:
        return "action_reset_conversation"

    def run(self, dispatcher, tracker, domain):
        # Reset the conversation
        
        return [FollowupAction("utter_greet"),Restarted()]