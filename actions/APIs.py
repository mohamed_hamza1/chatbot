import requests

class APIService:
    def __init__(self, base_url):
        self.base_url = base_url
        

    def _make_get_request(self, endpoint, params,header):
        try:
            response = requests.get(f"{self.base_url}{endpoint}", params=params,headers=header)
            response.raise_for_status()  # Raise an exception for non-200 status codes
            return response.json()
        except requests.exceptions.RequestException as e:
            return f"Error: {str(e)}"

    def _make_post_request(self, endpoint, json_data,header):
        try:
            response = requests.post(f"{self.base_url}{endpoint}", json=json_data,headers=header)
            response.raise_for_status()  # Raise an exception for non-200 status codes
            return response.json()
        except requests.exceptions.RequestException as e:
            return f"Error: {str(e)}"

    def send_customer_code(self, mobile_no, os_type, device_id):
        endpoint = "/api/Account/SendCustomerCode"
        params = {'mobileNo': mobile_no, 'OSType': os_type, 'deviceId': device_id}
        return self._make_get_request(endpoint, params,"")

    def verify_code(self, mobile_no, os_type, device_id, verify_code_request):
        endpoint = "/api/Account/GetToken"
        payload = {'mobileNo': mobile_no, 'code': verify_code_request, 'DeviceId': device_id, 'OSType': os_type}
        return self._make_post_request(endpoint, payload,"")

    def get_notifications(self, notifications_request):
        endpoint = "/api/Notification/GetNotificationRecords"
        return self._make_post_request(endpoint, notifications_request)

    def get_customer_addresses(self, mobile_number,token):
        endpoint = "/api/Account/GetCustomerAddresses"
        params = {'MobileNumbebr': mobile_number}
        headers = {
                "autheticationtoken": token,
                'Content-Type': 'application/json'
        }
        return self._make_get_request(endpoint, params,headers)

    def get_mappingDetails_City(self, city_name):
        endpoint = "/api/AddressDetailMapping/GetMappingDetails_Updated"
        params = {'CityName': city_name}
        return self._make_post_request(endpoint, params,"")
    
    def get_mappingDetails_Area(self, area_name):
        endpoint = "/api/AddressDetailMapping/GetMappingDetails_Updated"
        params = {'AreaName': area_name}
        return self._make_post_request(endpoint, params,"")
    
    def get_detailed_code(self, code,token):
        endpoint = "/api/DetailCode/GetDetailCode"
        params = {"MasterCodeId":code,"IsActive":True,"IncludeFieldDetail":True}
        headers = {
                "autheticationtoken": token,
                'Content-Type': 'application/json'
        }
        return self._make_post_request(endpoint, params,headers)

    def get_user_devices(self, mobile_number,families,token):
        endpoint = "/api/Device/GetUserServices"
        params = {"MobileNumber":mobile_number,"Families":families}
        headers = {
                "autheticationtoken": token,
                'Content-Type': 'application/json'
        }
        return self._make_post_request(endpoint, params,headers)

    def get_user_requests(self):
        endpoint = "/api/ServiceRequest/GetUserRequests"
        return self._make_get_request(endpoint)

    def create_service_request(self, service_request_data,token):
        endpoint = "/api/ServiceRequest/CreateServiceRequests"
        headers = {
                "autheticationtoken": token,
                'Content-Type': 'application/json'
        }
        return self._make_post_request(endpoint, service_request_data,headers)

    def cancel_requests(self, SRNumber,token):
        endpoint = "/api/ServiceRequest/CancelRequests"
        params = {"SRNumber":SRNumber}
        headers = {
                "autheticationtoken": token,
                'Content-Type': 'application/json'
        }
        return self._make_post_request(endpoint, params,headers)

    def upload_image(self, image_path):
        endpoint = "/api/FileHandler/UploadFile"
        files = {'SymptomImage': open(image_path, 'rb')}
        return self._make_post_request(endpoint, files)

    def get_available_appointments(self, service_request_number, is_initial, from_date, to_date):
        endpoint = "/api/Appointment/GetAvailableAppointmentWithFastVisit"
        params = {
            'SRNUmber': service_request_number,
            'isInitial': is_initial,
            'FromDate': from_date,
            'ToDate': to_date
        }
        return self._make_get_request(endpoint, params,"")

    def book_appointment(self, booking_appointment_request):
        endpoint = "/api/Appointment/BookAppointment"
        return self._make_post_request(endpoint, booking_appointment_request)

    def check_appointment_message(self, booking_appointment_request):
        endpoint = "/api/Appointment/CheckBrandMessage"
        return self._make_post_request(endpoint, booking_appointment_request)

    def get_available_appointments_days_count(self, key):
        endpoint = "/api/Configuration/GetConfigurationByKey"
        params = {'key': key}
        return self._make_get_request(endpoint, params,"")

    def get_available_service(self,token):
        endpoint = "/api/ServiceRequest/AvalibleServices"
        params = {}
        headers = {
                "autheticationtoken": token,
                'Content-Type': 'application/json'
        }
        return self._make_get_request(endpoint,params,headers)
    
    def get_packages(self):
        endpoint = "/api/Packages/GetPackages"
        params ={}
        return self._make_get_request(endpoint,params)

# Example usage

if __name__ == "__main__":
    def format_and_print_packages(package_data):
       for package in package_data:
        formatted_string = (
            f"Package ID: {package['id']}\n"
        #     f"Package Name (AR): {package['packageNameAr']}\n"
            f"Package Name: {package['packageNameEn']}\n"
        #     f"Package Description (AR): {package['packageDescriptionAr']}\n"
            f"Package Description: {package['packageDescriptionEn']}\n"
            f"Price: {package['Price']} SAR\n"
            "----------------------------------------\n"
        )
        print(formatted_string)
    base_url = "https://exmobileservices-test.azurewebsites.net/ExtraApi"
    token = "your_auth_token_here"
    
    scFieldProCleanService = "eff607b3-74f1-4a55-84fd-b6deb00697c1"
    masterCodeId = "9F9540BC-43FE-4E3B-9B36-22E11C781534"
    serviceTypeMasterCodeID = "52B9DFE5-FC0D-4248-B251-629203B2A1C8"
    service_request_service = APIService(base_url)
    mobile_no = "00966533168706"  # Replace with actual mobile number
    os_type = 2  # Replace with actual OS type (1 for iOS, 2 for Android, etc.)
    device_id = "your_device_id"  # Replace with actual device ID
    verify_code_request = "123456"
    user_requests_response = service_request_service.verify_code(mobile_no,2,"",123456)
    token = user_requests_response["Data"]["Token"]
    print(token)
    
    
    # user_requests_response = service_request_service.get_detailed_code(masterCodeId,token)
    
    user_requests_response = service_request_service.get_available_service(token)
    
    print(user_requests_response)
    
    # user_requests_response = service_request_service.get_user_devices(mobile_no,"AIR CONDITIONER-402",token)
    # print(user_requests_response)

