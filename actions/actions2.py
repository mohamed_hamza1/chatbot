# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import Restarted

import re


def validate_google_maps_url(url):
    pattern = r'https?://maps\.app\.goo\.gl/\w+'
    match = re.match(pattern, url)
    
    if match:
        return True
    else:
        return False

class ExtractService(Action):

    def name(self) -> Text:
        return "action_extract_service_entity"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        service_entity = next(tracker.get_latest_entity_values('service'),  None)
        
        if service_entity.lower() == 'cleaning':
            dispatcher.utter_message(text=f"you have selecetd {service_entity} as your service choice")        
        elif service_entity.lower() == 'maintenance':
            dispatcher.utter_message(text=f"you have selecetd {service_entity} as your service choice")
        return[]
    

class ValidateLocation(Action):

    def name(self) -> Text:
        return "action_location_validation"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
         # Get the latest message text
        latest_message = tracker.latest_message.get('text')
        print(latest_message)
        
        buttons = [{"title":"A/C Cleaning","payload":"/ac_cleaning"},{"title":"Device Maintenance","payload":"/device_maintenance"}]
        dispatcher.utter_button_message(text="Great! What kind of service are you looking for today?",buttons=buttons)
        # dispatcher.utter_image_url(image="https://images.unsplash.com/photo-1575936123452-b67c3203c357?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D")
        # if validate_google_maps_url(latest_message):
        #     # dispatcher.utter_message(text=f"Great! What kind of service are you looking for today?")
        # else:
        #     dispatcher.utter_message(text=f"you have entered invalid location")
        return[]
    
class ActionResetConversation(Action):
    def name(self) -> str:
        return "action_reset_conversation"

    def run(self, dispatcher, tracker, domain):
        # Reset the conversation
        return [Restarted()]